FROM gradle:5.2.1-jdk11-slim as build
COPY . /workspace/app
WORKDIR /workspace/app
USER root
RUN chown -R gradle /workspace/app
USER gradle
RUN gradle clean build -x test
RUN ls /workspace/app/build/libs/

FROM openjdk:11.0.3-jdk-slim
WORKDIR /workspace/app
COPY --from=build /workspace/app/build/libs/app.jar .
RUN ls /workspace/app/app.jar
EXPOSE 8081
CMD ["java","-jar", "/workspace/app/app.jar"]