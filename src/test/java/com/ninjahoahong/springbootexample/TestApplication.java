package com.ninjahoahong.springbootexample;

import org.springframework.boot.SpringApplication;

/**
 * Run this class from IDE instead of @link {@link Application} so that test class path
 * will be availale.
 *
 **/
public class TestApplication {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}