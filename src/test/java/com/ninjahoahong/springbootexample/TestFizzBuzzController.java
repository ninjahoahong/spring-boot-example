package com.ninjahoahong.springbootexample;

import com.ninjahoahong.springbootexample.jpa.UserRepository;
import com.ninjahoahong.springbootexample.model.request.LoginRequest;
import com.ninjahoahong.springbootexample.model.request.SignUpRequest;
import com.ninjahoahong.springbootexample.model.response.FizzBuzzResponse;
import com.ninjahoahong.springbootexample.model.response.JwtResponse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test FizzBuzzController
 */
public class TestFizzBuzzController extends TestApi {

    private static final String USER_NAME = "testuser";
    private static final String PASSWORD = "testpassword";


    @Autowired
    UserRepository userRepository;

    @Override
    @Before
    public void setup() throws Exception {
        super.setup();
    }

    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    @Test
    public void testUnAuthorizedRequest() throws Exception {
        String uriEmpty = "/api/v1/fizzbuzz?integerArray=";
        mvc.perform(get(uriEmpty)
            .accept(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(status().isUnauthorized())
            .andReturn();
    }

    @Test
    public void testBadRequest() throws Exception {
        String uriEmpty = "/api/v1/fizzbuzz?integerArray=1.1.1.1";
        mvc.perform(get(uriEmpty)
            .header(HttpHeaders.AUTHORIZATION, "Bearer " + getUserToken())
            .accept(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(status().isBadRequest())
            .andReturn();
    }

    @Test
    public void testEmptyInput() throws Exception {
        final FizzBuzzResponse EMPTY_RESPONSE = FizzBuzzResponse.builder()
            .fizzBuzzArray(new String[]{})
            .build();
        String uriEmpty = "/api/v1/fizzbuzz?integerArray=";
        MvcResult mvcResult = mvc.perform(get(uriEmpty)
            .header(HttpHeaders.AUTHORIZATION, "Bearer " + getUserToken())
            .accept(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(status().isOk())
            .andReturn();
        String content = mvcResult.getResponse().getContentAsString();
        FizzBuzzResponse fizzBuzzResponse = super.mapFromJson(content, FizzBuzzResponse.class);
        assertEquals(fizzBuzzResponse, EMPTY_RESPONSE);
    }

    @Test
    public void getFizzBuzzListV1() throws Exception {
        final FizzBuzzResponse EXPECTED_RESPONSE = FizzBuzzResponse.builder()
            .fizzBuzzArray(new String[]{"1", "2", "Buzz", "FizzBuzz", "Fizz"})
            .build();
        String uriDuplicate = "/api/v1/fizzbuzz?integerArray=1&integerArray=2&integerArray=100&integerArray=300&integerArray=3";
        MvcResult mvcResult = mvc.perform(get(uriDuplicate)
            .header(HttpHeaders.AUTHORIZATION, "Bearer " + getUserToken())
            .accept(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(status().isOk())
            .andReturn();

        String content = mvcResult.getResponse().getContentAsString();
        FizzBuzzResponse actualResponse = super.mapFromJson(content, FizzBuzzResponse.class);
        assertEquals(actualResponse, EXPECTED_RESPONSE);

        String urlComma = "/api/v1/fizzbuzz?integerArray=1,2,100,300,3";
        mvcResult = mvc.perform(get(urlComma)
            .header(HttpHeaders.AUTHORIZATION, "Bearer " + getUserToken())
            .accept(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(status().isOk())
            .andReturn();
        content = mvcResult.getResponse().getContentAsString();
        actualResponse = super.mapFromJson(content, FizzBuzzResponse.class);
        assertEquals(actualResponse, EXPECTED_RESPONSE);
    }

    private String getUserToken() throws Exception {
        SignUpRequest userSignUpRequest = SignUpRequest.builder()
            .name("test user")
            .username(USER_NAME)
            .password(PASSWORD)
            .email("email@email.com")
            .role(new HashSet<>(Arrays.asList("ROLE_USER"))).build();

        if (!userRepository.findByUsername(USER_NAME).isPresent()) {
            mvc.perform(MockMvcRequestBuilders.post("/api/v1/auth/signup")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapToJson(userSignUpRequest)))
                .andExpect(status().isOk())
                .andReturn();
        }

        LoginRequest loginRequest = LoginRequest.builder()
            .username(USER_NAME)
            .password(PASSWORD)
            .build();

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/api/v1/auth/login")
            .accept(MediaType.APPLICATION_JSON_VALUE)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(mapToJson(loginRequest)))
            .andExpect(status().isOk())
            .andReturn();

        JwtResponse jwtResponse = mapFromJson(result.getResponse().getContentAsString(), JwtResponse.class);
        return jwtResponse.getToken();
    }
}
