INSERT INTO roles (name)
    SELECT name FROM roles
    UNION
    VALUES ('ROLE_USER')
    EXCEPT
    SELECT name FROM roles;

INSERT INTO roles (name)
    SELECT name FROM roles
    UNION
    VALUES ('ROLE_ADMIN')
    EXCEPT
    SELECT name FROM roles;