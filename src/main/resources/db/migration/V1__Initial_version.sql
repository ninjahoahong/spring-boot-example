CREATE TABLE users (
  id       BIGSERIAL PRIMARY KEY,
  name     VARCHAR(100),
  username VARCHAR(100) UNIQUE,
  email    VARCHAR(100) UNIQUE,
  password VARCHAR(100) NOT NULL
);

CREATE TYPE role_name AS ENUM ('ROLE_USER', 'ROLE_ADMIN');

CREATE TABLE roles (
  id             BIGSERIAL PRIMARY KEY,
  name           role_name
);

INSERT INTO roles (name) VALUES ('ROLE_USER');

INSERT INTO roles (name) VALUES ('ROLE_ADMIN');

CREATE TABLE users_roles (
  user_id     BIGSERIAL REFERENCES users (id) ON UPDATE CASCADE ON DELETE CASCADE,
  role_id     BIGSERIAL REFERENCES roles (id) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT  users_roles_pkey PRIMARY KEY (user_id, role_id)
);