package com.ninjahoahong.springbootexample;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Generic application configuration and beans. This file should be in root package.
 */
@Configuration
@EnableAsync
@EnableScheduling
@ComponentScan
public class ApplicationConfig {

}