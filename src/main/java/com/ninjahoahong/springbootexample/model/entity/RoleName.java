package com.ninjahoahong.springbootexample.model.entity;

/**
 * Custom defined role names
 */
public enum  RoleName {
    ROLE_USER,
    ROLE_ADMIN
}