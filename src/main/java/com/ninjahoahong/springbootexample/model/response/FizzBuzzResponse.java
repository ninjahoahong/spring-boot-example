package com.ninjahoahong.springbootexample.model.response;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/**
 * Response Object for fizzbuzz api
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FizzBuzzResponse {

    @NonNull
    private String[] fizzBuzzArray;
}
