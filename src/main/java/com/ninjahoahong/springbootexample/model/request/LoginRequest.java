package com.ninjahoahong.springbootexample.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class LoginRequest {
    @NotBlank
    @Size(min=3, max = 100)
    private String username;
 
    @NotBlank
    @Size(min = 6, max = 100)
    private String password;
}