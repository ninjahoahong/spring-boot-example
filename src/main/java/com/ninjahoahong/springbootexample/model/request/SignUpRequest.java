package com.ninjahoahong.springbootexample.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class SignUpRequest {
    @NotBlank
    @Size(min = 3, max = 100)
    private String name;
 
    @NotBlank
    @Size(min = 3, max = 100)
    private String username;
 
    @NotBlank
    @Size(max = 100)
    @Email
    private String email;
    
    private Set<String> role;
    
    @NotBlank
    @Size(min = 6, max = 100)
    private String password;
}