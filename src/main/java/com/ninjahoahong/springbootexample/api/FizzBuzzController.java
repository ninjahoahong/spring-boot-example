
package com.ninjahoahong.springbootexample.api;

import com.ninjahoahong.springbootexample.model.response.FizzBuzzResponse;
import io.swagger.annotations.Api;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import java.util.Arrays;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Controller to provide service to calculate fizzbuzz
 */

@RestController
@RequestMapping(path = "/api")
@Api(description = "Fizz Buzz generator api.")
public class FizzBuzzController {
    @RequestMapping(
        method = GET,
        path = "/v1/fizzbuzz",
        produces = {MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public ResponseEntity<FizzBuzzResponse> getFizzBuzzListV1(@RequestParam("integerArray") int[] integerArray) {
        return ResponseEntity.ok(FizzBuzzResponse.builder()
            .fizzBuzzArray(calculateFizzBuzzArray(integerArray))
            .build());
    }

    private String[] calculateFizzBuzzArray(int[] integerArray) {
        return Arrays.stream(integerArray).mapToObj(
            i -> i % 3 == 0 ?
                (i % 5 == 0 ? "FizzBuzz" : "Fizz") :
                (i % 5 == 0 ? "Buzz" : String.valueOf(i))).toArray(String[]::new);
    }
}