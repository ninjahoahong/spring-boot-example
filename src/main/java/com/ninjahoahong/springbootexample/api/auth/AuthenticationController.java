package com.ninjahoahong.springbootexample.api.auth;

import com.ninjahoahong.springbootexample.jpa.RoleRepository;
import com.ninjahoahong.springbootexample.jpa.UserRepository;
import com.ninjahoahong.springbootexample.model.entity.Role;
import com.ninjahoahong.springbootexample.model.entity.RoleName;
import com.ninjahoahong.springbootexample.model.entity.User;
import com.ninjahoahong.springbootexample.model.request.LoginRequest;
import com.ninjahoahong.springbootexample.model.request.SignUpRequest;
import com.ninjahoahong.springbootexample.model.response.JwtResponse;
import com.ninjahoahong.springbootexample.security.JwtProvider;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;

/**
 * Rest apis to signup and login
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api")
@Api(description = "Sign up and login api.")
public class AuthenticationController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtProvider jwtProvider;

    @PostMapping("v1/auth/login")
    public ResponseEntity<?> loginV1(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
            new UsernamePasswordAuthenticationToken(
                loginRequest.getUsername(),
                loginRequest.getPassword()
            )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtProvider.generateJwtToken(authentication);
        return ResponseEntity.ok(JwtResponse.builder().token(jwt).type("Bearer").build());
    }

    @PostMapping("v1/auth/signup")
    public ResponseEntity signUpV1(@Valid @RequestBody SignUpRequest signUpRequest) {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return new ResponseEntity<String>("Username is already taken!",
                HttpStatus.BAD_REQUEST);
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity<String>("Email is already in use!",
                HttpStatus.BAD_REQUEST);
        }

        User newUser = User.builder()
            .name(signUpRequest.getName())
            .username(signUpRequest.getUsername())
            .email(signUpRequest.getEmail())
            .password(encoder.encode(signUpRequest.getPassword()))
            .build();

        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();

        strRoles.forEach(role -> {
            switch (role) {
                case "admin":
                    Role adminRole = roleRepository.findByName(RoleName.ROLE_ADMIN)
                        .orElseThrow(() -> new RuntimeException("User Role not find."));
                    roles.add(adminRole);
                    break;

                default:
                    Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
                        .orElseThrow(() -> new RuntimeException("User Role not find."));
                    roles.add(userRole);
            }
        });

        newUser.setRoles(roles);
        userRepository.save(newUser);

        return ResponseEntity.ok().build();
    }
}