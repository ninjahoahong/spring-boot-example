# Spring Boot Example

This is spring boot example with gradle as a build tool.

## Development Setup

1. Install Intellij-Idea-CE
2. Import project
3. Set up run configuration like in the following image:


![run-configuration](run-configuration.png)

## Build
`docker-compose up -d --build`

clean up: `docker-compose down -v --rmi all --remove-orphans`

## Test
`./gradlew test`

* Test fizz buzz api with swagger-ui: 
    1. Access to `/swagger-ui.html`.
    2. Create account with `signup` api.
    3. Login using `login` api and copy the `token`.
    4. Authorize by the token from step 3 like in the following picture (type "Bearer " then paste the token next to it)
    
    ![run-configuration](authorize-to-swagger.png)
 

## Dependencies

* Actuator for management
* lombok for easy annotations
* swagger and swagger-ui for docs
* devtools for dev
* security for signup and login
* postgresql for database
* jpa for database access
* jjwt for jwt in java
* h2 for database using in test
* security test for testing security stuffs
* boot-starter-test for testing boot application


## TODO

* Audio stream with Socket.
* Chat with web socket.
* Client implementation with Vuejs.

## Step by steps guide to start similar spring boot project

1. Initialize with [Spring Initializr](https://start.spring.io/). Gradle project, dependencies: Actuator, web, devtools, 
security, lombok. Copy the `build.gradle` file from [here](https://github.com/spring-guides/gs-rest-service/blob/master/initial/build.gradle).
Add swagger and swagger-ui dependencies.

2. Install docker

## Resources

1. [Spring guides](https://spring.io/guides)
2. [Beadung site](https://www.baeldung.com/) 
3. [Ruslan's Spring template with maven](https://github.com/huksley/spring-boot2-template)